# Question
def input_date():
    Date = input("What date would you 'really' like to know how many seconds we are from (DD/MM/YYYY): ")

    return Date


# Convert to Date
def str_to_date(Date):
    """
    This function is meant to do a ton of blaaahhh. Have a nice day
    :rtype: object
    :param Date: 
    :return: a date in dtetime
    """
    from datetime import datetime
    Date = datetime.strptime(Date, '%d/%m/%Y')

    return Date


# Convert Date to Seconds
def cal_secs(Date):
    from datetime import datetime

    Today = datetime.today()

    Duration = Today - Date

    Seconds = Duration.total_seconds()

    return Seconds
